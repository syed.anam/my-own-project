<?php
include 'inc/header.php';
include './lib/student.php';
?>

<?php
$stu = new Student();
if($_SERVER['REQUEST_METHOD'] == 'POST'){
  $name = $_POST['name'];
  $roll = $_POST['roll'];
  $insert_data = $stu->insert_student($name, $roll);
}
?>

<?php
if (isset($insert_data)){
  echo $insert_data;
}
?>

<div class="panel panel-default">
  <div class="panel panel-heading">
    <h2>
      <a href="add.php" class="btn btn-success" >Add Student </a>
      <a href="index.php" class="btn btn-info pull-right" >Back</a>
    </h2>
  </div>

  <div class="panel-body"> 
    <form action="" method="post">
      <div class="form-group">
        <label for="name">Student Name</label>
        <input type="text" name="name" id="name"  class="form-control"/>
      </div>
      <div class="form-group">
        <label for="roll">Student Roll</label>
        <input type="text" name="roll" id="roll"  class="form-control"/>
      </div>
      <div class="form-group">
        
        <input type="submit" name="submit" value="Submit" class="btn btn-success"/>
      </div>
    </form>
  </div>
</div>

</div>
</body>
</html>

