<?php
include 'inc/header.php';
include 'lib/student.php';
?>

<div class="panel panel-default">
  <div class="panel panel-heading">
    <h2>
      <a href="add.php" class="btn btn-success" >Add Student </a>
      <a href="index.php" class="btn btn-info pull-right" >Take Attendance</a>
    </h2>
  </div>

  <div class="panel-body"> 
    <form action="" method="post">
      <table class="table table-striped">
        <tr>
          <th width="30%">SI</th>
          <th width="50%">Attendance Date</th>
          <th width="20%">Action</th>      
        </tr>
        <?php
        $stu  = new Student();
        $get_date = $stu->get_date_list();
        if ($get_date) {
          $i = 0;
          while ($value = $get_date->fetch_assoc()) {
            $i++;
            ?>
            <tr>
              <td><?php echo $i;?></td>
              <td><?php echo $value['att_time'];?></td>
              <td>
                <a href="student_view.php?dt=<?php echo $value['att_time'];?>" class="btn btn-info">View</a>
              </td>             
            </tr>              
            <?php
          }
        }
        ?>
        <tr>
          <td><input type="submit" name="submit" value="Submit" class="btn btn-primary btn-block"/></td>
        </tr>

      </table>

    </form>
  </div>
</div>

</div>
</body>
</html>
