-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2016 at 09:01 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ams`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_attend`
--

CREATE TABLE IF NOT EXISTS `tbl_attend` (
  `id` int(11) NOT NULL,
  `roll` int(11) NOT NULL,
  `attend` varchar(100) NOT NULL,
  `att_time` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_attend`
--

INSERT INTO `tbl_attend` (`id`, `roll`, `attend`, `att_time`) VALUES
(1, 100, '', '2016-12-03'),
(2, 101, '', '2016-12-03'),
(3, 102, '', '2016-12-03'),
(4, 103, '', '2016-12-03'),
(5, 105, '', '2016-12-03'),
(6, 104, '', '2016-12-03'),
(7, 106, '', '2016-12-03'),
(8, 100, 'present', '2016-12-04'),
(9, 101, 'present', '2016-12-04'),
(10, 102, 'absent', '2016-12-04'),
(11, 103, 'absent', '2016-12-04'),
(12, 105, 'present', '2016-12-04'),
(13, 104, 'present', '2016-12-04'),
(14, 106, 'absent', '2016-12-04'),
(15, 100, 'present', '2016-12-05'),
(16, 101, 'present', '2016-12-05'),
(17, 102, 'present', '2016-12-05'),
(18, 103, 'present', '2016-12-05'),
(19, 105, 'present', '2016-12-05'),
(20, 104, 'present', '2016-12-05'),
(21, 106, 'present', '2016-12-05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE IF NOT EXISTS `tbl_student` (
  `std_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `roll` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`std_id`, `name`, `roll`) VALUES
(1, 'ashik', 100),
(2, 'shapan', 101),
(3, 'mofiz', 102),
(4, 'kuddus', 103),
(5, 'moynul', 105),
(7, 'abdur rahim', 104),
(9, 'samim', 106);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_attend`
--
ALTER TABLE `tbl_attend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`std_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_attend`
--
ALTER TABLE `tbl_attend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `std_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
