<?php

$filepath = realpath(dirname(__FILE__));
include_once ($filepath . '/database.php');
?>

<?php

class Student {

  private $db;

  public function __construct() {
    $this->db = new Database();
  }

  public function get_student() {
    $query = "SELECT * FROm tbl_student";
    $result = $this->db->select($query);
    return $result;
  }

  public function insert_student($name, $roll) {
    $name = mysqli_real_escape_string($this->db->link, $name);
    $roll = mysqli_real_escape_string($this->db->link, $roll);
    if (empty($name) || empty($roll)) {
      $msg = "Error ! Field Must not be Empty!";
      return $msg;
    } else {
      $stu_query = "INSERT INTO tbl_student (name, roll) VALUES ('$name','$roll')";
      $stu_insert = $this->db->insert($stu_query);

      $stu_query = "INSERT INTO tbl_attend ( roll) VALUES ('$roll')";
      $stu_insert = $this->db->insert($stu_query);

      if ($stu_insert) {
        $msg = "Success! Student data insert Successfully";
        return $msg;
      } else {
        $msg = "Error! Student data NOT insert ";
        return $msg;
      }
    }
  }

  public function insert_attend($cur_date, $attend = array()) {
    $query = "SELECT DISTINCT att_time FROM tbl_attend";
    $get_data = $this->db->select($query);

    while ($result = $get_data->fetch_assoc()) {
      $db_date = $result['att_time'];
      if ($cur_date == $db_date) {
        $msg = "Attendance already taken";
        return $msg;
      }
    }

    foreach ($attend as $att_key => $att_value) {
      if ($att_value == 'present') {
        $stu_query = "INSERT INTO tbl_attend (roll, attend, att_time) VALUES ('$att_key','present',now())";
        $data_insert = $this->db->insert($stu_query);
      } elseif ($att_value == 'absent') {
        $stu_query = "INSERT INTO tbl_attend (roll, attend, att_time) VALUES ('$att_key','absent',now())";
        $data_insert = $this->db->insert($stu_query);
      }
    }

    if ($data_insert) {
      $msg = "Success! Attendance data insert Successfully";
      return $msg;
    } else {
      $msg = "Error! $data_insert data NOT insert ";
      return $msg;
    }
  }
  
  public function get_date_list(){
    $query = "SELECT DISTINCT att_time FROM tbl_attend";
    $result = $this->db->select($query);
    return $result;
  }
  
  public function get_all_data($dt){
    $query = "SELECT tbl_student.name,  tbl_attend.* "
            . "FROM  tbl_student "
            . "INNER JOIN tbl_attend "
            . "On tbl_student.roll = tbl_attend.roll "
            . "WHERE att_time = '$dt' ";
     $result = $this->db->select($query);
    return $result;
  }
  
  public function update_attend($dt, $attend){
    foreach ($attend as $att_key => $att_value) {
      if ($att_value == 'present') {
        $stu_query = "UPDATE tbl_attend SET attend = 'present' WHERE roll = '{$att_key}' AND att_time ='{$dt}' ";
        $data_update = $this->db->insert($stu_query);
      } elseif ($att_value == 'absent') {
        $stu_query = "UPDATE tbl_attend SET attend = 'absent' WHERE roll = '{$att_key}' AND att_time ='{$dt}' ";
        $data_update = $this->db->insert($stu_query);
      }
    }

    if ($data_update) {
      $msg = "Success! Attendance data Update Successfully";
      return $msg;
    } else {
      $msg = "Error! Attendance data NOT Update ";
      return $msg;
    }
  }

}
