<?php
include 'inc/header.php';
include 'lib/student.php';
?>
<script>
//  $(document).ready(function(){
//    $("form").submit(function(){
//      var roll = true;
//      $(':radio').each(function(){
//        name = $(this).attr('name');
//        if(roll && !$(':radio[name=" '+name+' "]:checked').length){
////          alert(name + " Roll Missing");
//            $('.alert').show();
//          roll = false;
//        }
//      });
//      return roll;
//    });
//  });
  </script>
<?php
//error_reporting(0);
$stu = new Student();
$dt = $_GET['dt'];
if($_SERVER['REQUEST_METHOD'] == 'POST'){  
  $attend = $_POST['attend'];
  $update_attend = $stu->update_attend($dt, $attend);
}
?>
<?php
if(isset($update_attend)){
  echo $update_attend;
}
?>


<div class="panel panel-default">
  <div class="panel panel-heading">
    <h2>
      <a href="add.php" class="btn btn-success" >Add Student </a>
      <a href="date_view.php" class="btn btn-info pull-right" >View  All</a>
    </h2>
  </div>

  <div class="panel-body">
    <div class="well text-center">
      <h3>
        <strong>DATE: </strong> <?php echo $dt; ?>
      </h3>
    </div>

    <form action="" method="post">
      <table class="table table-striped">
        <tr>
          <th width="25%">SI</th>
          <th width="25%">Student Name</th>
          <th width="25%">Roll</th>
          <th width="25%">Attendance</th>
        </tr>
        <?php
        $get_data = $stu->get_all_data($dt);
        if ($get_data) {
          $i = 0;
          while ($value = $get_data->fetch_assoc()) {
            $i++;
            ?>
            <tr>
              <td><?php echo $i;?></td>
              <td><?php echo $value['name'];?></td>
              <td><?php echo $value['roll'];?></td>
              <td>
                <input type="radio" name="attend[<?php echo $value['roll'];?>]" value="present" 
                  <?php if ($value['attend'] == 'present'){ echo 'checked';} ?> />P
                <input type="radio" name="attend[<?php echo $value['roll'];?>]" value="absent" 
                  <?php if ($value['attend'] == 'absent'){echo 'checked';} ?>/>A
              </td>
            </tr>              
            <?php
          }
        }
        ?>
        <tr>
          <td><input type="submit" name="submit" value="update" class="btn btn-primary"/></td>
        </tr>

      </table>

    </form>
  </div>
</div>

</div>
</body>
</html>
